#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Nov  2 22:54:43 2018

@author: Gerardo A. Rivera Tello
"""

import os
import gsw
import xarray as xr
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from scipy.sparse import diags
from numba import jit
from dask.diagnostics import ProgressBar


# %%
def global_mean(files_path, var, lati, long, time):
    with xr.open_mfdataset(files_path) as ds:
        if time is None:
            reg = ds.sel(lat=slice(lati[0], lati[1]),
                         lon=slice(long[0], long[1]))
        else:
            reg = ds.sel(lat=slice(lati[0], lati[1]),
                         lon=slice(long[0], long[1]),
                         time=slice(np.datetime64(time[0]),
                                    np.datetime64(time[1]))
                         )
    time = '{}-{}'.format(pd.to_datetime(reg.time.data.min()).year,
                          pd.to_datetime(reg.time.data.max()).year)
    data = reg[var]
    # Data mean with nan values where required
    data = data.mean(dim=['lat', 'time'], skipna=False)
    data.attrs = reg[var].attrs
    return data, time


# %%
@jit(parallel=True, nogil=True)
def interp_data(xarr, step, surface, bottom):
    # Interpolate nan values first
    cubic = xarr.interpolate_na(dim='level', fill_value='extrapolate')

    # Interpolate to fit on a regular grid
    cubic = cubic.interp(level=np.arange(surface, bottom+1, step),
                         method='linear',
                         kwargs={'fill_value': 'extrapolate'})
    cubic.level.attrs = xarr.level.attrs
    cubic.attrs['long_name'] = 'Interpolated {}'.format(
                                                    xarr.attrs['long_name'])
    return cubic


# %%
def plotvar(var1, var2, y_ax, size):
    if var2 is None:
        fig, ax_ = plt.subplots(figsize=size)
        var1.plot(y=y_ax, yincrease=False, ax=ax_)
        ax_.margins(y=0)
        ax_.grid(ls='--')
        fig.tight_layout()
    else:
        fig, ax_ = plt.subplots(ncols=2, figsize=size)
        var1.plot(y=y_ax, yincrease=False, ax=ax_[0])
        var2.plot(y=y_ax, yincrease=False, ax=ax_[1])
        for axis in ax_:
            axis.margins(y=0)
            axis.grid(ls='--')
        fig.tight_layout()


def plotmodes(vmodes, num, size):
    fig, ax_ = plt.subplots(figsize=size)
    for n_ in range(num):
        vmodes[0][:, n_].plot(y='mid_level', yincrease=False, ax=ax_)
    ax_.margins(y=0)
    ax_.grid(ls='--')
    fig.tight_layout()


# %%
@jit(nopython=True, parallel=True, nogil=True)
def alpha(nsquared, step):
    return 1/(nsquared*np.power(step, 2))


@jit(parallel=True, nogil=True)
def build_mx(alpha_val, step):
    coef_mx = np.zeros((alpha_val.shape[1],
                        alpha_val.shape[0]-1,
                        alpha_val.shape[0]-1))
    for num in range(alpha_val.shape[1]):
        coef_mx[num] = diags([-alpha_val[1:, num]-alpha_val[:-1, num],
                             alpha_val[1:-1, num],
                             alpha_val[1:-1, num]], [0, -1, 1]).toarray()
        temp = 2*alpha_val[0, num]*(step)*(9.8)
        t_coef = 1+((1+temp)/(1-temp))
        coef_mx[num, 0, 0] = (-alpha_val[0, num]*t_coef)-alpha_val[1, num]
        coef_mx[num, 0, 1] = alpha_val[1, num]
        coef_mx[num, -1, -2] = alpha_val[-3, num]
        coef_mx[num, -1, -1] = alpha_val[-3, num]*-1
    return coef_mx


# %%
@jit(parallel=True, nogil=True)
def ord_eig(mat):
    e_val, e_vect = np.linalg.eig(mat)
    ce_num = np.absolute(np.sqrt(np.complex128(-1/e_val)))
    ixsort = ce_num.argsort()
    for num in range(e_val.shape[0]):
        ce_num[num] = ce_num[num][ixsort[num][::-1]]
        e_vect[num] = e_vect[num][:, ixsort[num][::-1]]
    return ce_num, e_vect


# %%
def check_file(name, time):
    if type(time) is tuple:
        time = '{}-{}'.format(pd.to_datetime(time[0]).year,
                              pd.to_datetime(time[1]).year)
    f_path = os.path.join(os.getcwd(), 'output/{}_{}.nc'.format(name, time))
    return {os.path.basename(f_path): os.path.exists(f_path)}


def save_nc(outdir, xarr, t_range):
    xarr.to_netcdf(os.path.join(outdir,
                                '{}_{}.nc'.format(xarr.name, t_range)), 'w')


# %%
def main(tempf, saltf, lat, surf, depth, step, hmix, lon=(160, 270),
         time=None):
    # Data computation
    temp, t_range = global_mean(tempf, 'pottmp', lat, lon, time)
    salt, t_range = global_mean(saltf, 'salt', lat, lon, time)

    # Data Loading
    with ProgressBar():
        print('Loading Temperature data')
        temp.load()
        print('Loading Salinity data')
        salt.load()

    # Cubic Interpolation
    temp_cs = interp_data(temp, step, surf, depth)
    salt_cs = interp_data(salt, step, surf, depth)

    # Temperature and Salinity variables
    TEMP = temp_cs
    SALT = salt_cs
    # Original and Interp Data Plotting
#    plotvar(temp, temp_cs, 'level', (8, 8))
#    plotvar(salt, salt_cs, 'level', (8, 8))

    # Temperature in degrees celcius
    c_temp = TEMP-273
    c_temp.attrs = TEMP.attrs
    c_temp.attrs['units'] = 'C'

    # Conservative temperature computation
    ct = gsw.conversions.CT_from_pt(SALT, c_temp)
    # Pressure computation
    press = gsw.conversions.p_from_z(-TEMP.level.data, 0)
    press = np.repeat(press[None, :], TEMP.shape[1], axis=0).T
    # N_squared computation
    n_squared, press_mid = gsw.Nsquared(SALT*1000, ct, press, 0)
    # Save in xArray
    mid_depth = (TEMP.level.data[:-1]+TEMP.level.data[1:])/2
    n_squared = xr.DataArray(n_squared,
                             coords=[mid_depth, TEMP.lon],
                             dims=['mid_level', 'lon'],
                             name='n_squared')
    n_squared = n_squared.to_pandas()
    n_squared = xr.DataArray(n_squared.rolling(15,
                                               center=True,
                                               min_periods=1).mean(),
                             name='n_squared',
                             attrs={'long_name':
                                    'Brunt-Vaisala Frequency squared',
                                    'units': '1/s^2'})
    n_squared.mid_level.attrs = TEMP.level.attrs
    n_squared.lon.attrs = TEMP.lon.attrs

    # Normal Modes computation
    alp = alpha(n_squared.data, step)
    coef = build_mx(alp, step)
    print("Obtaining modes")
    vel, vect = ord_eig(coef)
    print("Done")
    vel = xr.DataArray(vel,
                       coords=[TEMP.lon, range(vel.shape[1])],
                       dims=['lon', 'mode'],
                       name='phase_speed',
                       attrs={'long_name': 'Phase speed',
                              'units': 'm/s'})
    vel.lon.attrs = TEMP.lon.attrs
    vect = xr.DataArray(vect,
                        coords=[TEMP.lon, mid_depth[1:], range(vel.shape[1])],
                        dims=['lon', 'mid_level', 'mode'],
                        name='normal_modes',
                        attrs={'long_name': 'Normal Vertical Modes'})
    vect.mid_level.attrs = TEMP.level.attrs
    norm = vect/vect[:, 0]
    norm.attrs['long_name'] = 'Normalized Vertical Modes'
    norm.mid_level.attrs = TEMP.level.attrs

    # Projection coefficients computation
    den_ = np.trapz(np.power(norm, 2), dx=step, axis=1)
    num_ = np.trapz(norm.sel(mid_level=slice(hmix)), dx=step, axis=1)
    proj = num_/(hmix*den_)
    proj = xr.DataArray(proj,
                        coords=[TEMP.lon, range(vel.shape[1])],
                        dims=['lon', 'mode'],
                        name='projection_coef',
                        attrs={'long_name': 'Projection Coefficient'})
    proj.lon.attrs = TEMP.lon.attrs

    # Save data to netcdf
    outdir = os.path.join(os.getcwd(), 'output')
    save_nc(outdir, n_squared, t_range)
    save_nc(outdir, vel, t_range)
    save_nc(outdir, norm.sel(mode=slice(10)), t_range)
    save_nc(outdir, proj, t_range)
    save_nc(outdir, c_temp, t_range)
    save_nc(outdir, SALT, t_range)
#    return n_squared, vel, norm, proj


# %%
def run(tempf, saltf, lat, surf, depth, step, hmix,
        name=['n_squared', 'phase_speed', 'normal_modes',
              'projection_coef', 'pottmp', 'salt'],
        lon=(160, 270), time=None):
    # Set time defaults in str format
    time1 = False
    if time is None:
        time = '{}-{}'.format(os.path.basename(tempf[0])[7:-3],
                              os.path.basename(tempf[-1])[7:-3])
        time1 = None
    print('### Time range set: {} ###'.format(time))
    print('Looking for data containers\n')
    exists = {key: value for var in name
              for key, value in check_file(var, time).items()}
    for k, v in exists.items():
        print('{0:40} {1}'.format(k, v))
    if False not in exists.values():
        print("\nData container already exists.\nSkipping computation.\n")
    else:
        print('\nFiles not found or at least one file is missing')
        print('Starting computation.\n')
        if time1 is None:
            main(tempf, saltf, lat, surf, depth, step, hmix, lon, time1)
        else:
            main(tempf, saltf, lat, surf, depth, step, hmix, lon, time)
    print('Loading data')
    n_squared = xr.open_dataarray(['output/{}'.format(val)
                                  for val in exists.keys()
                                  if 'n_sq' in val][0])
    vel = xr.open_dataarray(['output/{}'.format(val) for val in exists.keys()
                            if 'phase' in val][0])
    norm = xr.open_dataarray(['output/{}'.format(val) for val in exists.keys()
                             if 'normal' in val][0])
    proj = xr.open_dataarray(['output/{}'.format(val) for val in exists.keys()
                             if 'proj' in val][0])
    temp = xr.open_dataarray(['output/{}'.format(val) for val in exists.keys()
                             if 'tmp' in val][0])
    salt = xr.open_dataarray(['output/{}'.format(val) for val in exists.keys()
                             if 'salt' in val][0])
    print('Done\n')
    return n_squared, vel, norm, proj, temp, salt


# %%
if __name__ == '__main__':
    # Variables Definition
    DEPTH = 4500
    STEP = 5
    LAT = (-0.2, 0.2)
#    LON = (150, 270)
    SURF = 0
    HMIX = 150

    SALT_DIR = "/home/DangoMelon/Documents/GODAS/salinity/"
    TEMP_DIR = "/home/DangoMelon/Documents/GODAS/temperature/"
    SALT_FILES = [os.path.join(SALT_DIR, x)
                  for x in os.listdir(SALT_DIR) if x.endswith('.nc')]
    TEMP_FILES = [os.path.join(TEMP_DIR, x)
                  for x in os.listdir(TEMP_DIR) if x.endswith('nc')]
    TEMP_FILES.sort()
    SALT_FILES.sort()
# %%
    # Data computation
    n_squared, vel, norm, proj = run(TEMP_FILES, SALT_FILES, LAT, SURF,
                                     DEPTH, STEP, HMIX)
# %%
    # N_squared plot
    plotvar(n_squared, None, y_ax='mid_level', size=(4, 8))

    # Plot Modes
#    plotmodes(vect, 5, (4, 8))
    plotmodes(norm, 6, (5, 8))
    plotvar(proj, None, y_ax='mode', size=(4, 8))
    n_squared.close()
    vel.close()
    norm.close()
    proj.close()
